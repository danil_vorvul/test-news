from django.db import models
from django.contrib.auth.models import User


class MyUser(models.Model):
    """
    Extended User's model
    """
    ADMIN = ['Admin', ]
    EDITOR = ['Editor', ]
    CUSTOM_USER = ['Custom_user', ]
    user = models.OneToOneField(User, related_name='profile', on_delete=models.CASCADE)
    birth_day = models.DateField(auto_created=True)

    def __str__(self):
        return '{} {} {}'.format(self.user.first_name, self.user.last_name, self.user.username)

    class Meta:
        verbose_name = "MyUser"
        verbose_name_plural = "MyUsers"
