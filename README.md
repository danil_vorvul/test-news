Project name: Test News. Creating news site with Celery, RabbitMQ and deploy into AWS EC2 with Linux AMI

## Installation
Install pip and virtualenv
```
sudo yum update
sudo yum install python3-pip
sudo yum install git
sudo pip3 install virtualenv
```

Download project from git
```
git clone "https://gitlab.com/danil_vorvul/test-news.git"
```
Create and activate virtualenv
```
virtualenv news
source news/bin/activate
```

##### Install python packages

```
pip3 install -r requirements.txt
```

Create groups for permissions

```
python manage.py create_groups
```

Run Django localhost

```
python manage.py runserver 0.0.0.0:80
```

##### Run Celery

```
celery -A news worker -l info
```

##### Run RabbitMQ
link to rabbit mq instalation into Centos 7:
https://saqot.com/working/rabbitmq-queue-bundle/setup-rabbitmq-centos7.html
```
systemctl start rabbitmq-server
```
## Run tests
```
python manage.py test board.tests
```
